#!/bin/bash
fPath=$PWD"/certs"
if [ ! -d $fPath ]; then
  mkdir 777 $fPath
fi

openssl req -x509 -nodes -days 365 \
  -subj "/C=CA/ST=QC/O=Company, Inc./CN=balancer" \
  -addext "subjectAltName=DNS:balancer" \
  -newkey rsa:2048 \
  -keyout $fPath"/balancer.key" \
  -out $fPath"/balancer.crt"
