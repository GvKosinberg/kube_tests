const express = require('express');
const axios = require('axios');
const app = express();
const port = 80


app.get('/', (req, res) => {
  res.send('Hello from proxy')
})

app.get('/get-service/:service_id', async (req, res) => {
  const service_id = req.params.service_id;
  try {
    const URL = `http://service_${service_id}`;
    const response = await axios.get(URL);
    return res.send(response.data);
  } catch (e) {
    console.error(e);
    return res
      .status(500)
      .send(`Can not get data from service ${service_id}`)
  }
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
