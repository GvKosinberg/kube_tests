module.exports = {
  "devServer": {
    "https": false,
    "disableHostCheck": true,
    "proxy": {
      "/proxy": {
          "target": 'http://proxy',
          "changeOrigin": true,
          "pathRewrite": {
            "^/proxy": ""
          }
      }
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}
