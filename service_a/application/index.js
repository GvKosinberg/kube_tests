const express = require('express')
const axios = require('axios');
const fs = require('fs');
const https = require('https');
const app = express()
const port = 80

const CERTS_ROOT = '/home/node/certs/certs';
// const CA_PATH = `${CERTS_ROOT}/rootCA.crt`
const CERT_PATH = `${CERTS_ROOT}/balancer.crt`
const trustedCa = [
  CERT_PATH
  // CERT_PATH
];

https.globalAgent.options.ca = [];
for (const ca of trustedCa) {
    https.globalAgent.options.ca.push(fs.readFileSync(ca));
}
// const httpsAgent = new Agent({
//   // rejectUnauthorized: false,
//   ca: fs.readFileSync(CERT_PATH),
//   // cert: fs.readFileSync(CERT_PATH),
//   // key: fs.readFileSync(KEY_PATH),
// })
// axios.defaults.options = httpsAgent

app.get('/', (req, res) => {
  res.send('Hello from service a!')
})

app.get('/b', async (req, res) => {
  try {
    const b_resp = await axios.get('http://service_b/c')
    res.send(b_resp.data)
  } catch (e) {
    console.error(e);
    res.status(500).send('Screwed up')
  }
})

app.get('/b/c/d/e', async (req, res) => {
  try {
    const b_resp = await axios.get('https://balancer/api/service_b/')
    res.send(b_resp.data)
    // res.send('Such query wow')
  } catch (e) {
    console.error(e.message);
    res.status(500).send('Screwed up')
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
