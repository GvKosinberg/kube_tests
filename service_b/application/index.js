const express = require('express')
const app = express()
const port = 80

app.get('/', (req, res) => {
  console.log(`SERVICE_B: index`);
  res.send('Hello from service B!')
})

app.get('/c', (req, res) => {
  console.log(`SERVICE_B: redir`);

  res.send('Redirrected data')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
