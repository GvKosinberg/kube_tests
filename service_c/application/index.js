const express = require('express');
const axios = require('axios');
const app = express()
const port = 80

app.get('/', (req, res) => {
  console.log(`SERVICE_C: index`);
  res.send('Hello from service C!')
})

app.get('/d', (req, res) => {
  console.log(`SERVICE_C: d`);
  res.send('Redirrected data')
})

app.get('/ask_b', async (req, res) => {
  console.log(`SERVICE_C: ASKING B`);
  const response = await axios.get('http://service_b/');
  res.send(response.data)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
